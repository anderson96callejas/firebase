import { ConsultarInformeComponent } from './components/consultar-informe/consultar-informe.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { IndexComponent } from './components/index/index.component';
import { LoginComponent } from './components/users/login/login.component';
import { RegisterComponent } from './components/users/register/register.component';
import { ProfileComponent } from './components/users/profile/profile.component';
import { Page404Component } from './components/page404/page404.component';
import { CompararInformeComponent } from './components/comparar-informe/comparar-informe.component';

const routes: Routes = [
  { path: '', component: IndexComponent },
  { path: 'user/login', component: LoginComponent},
  { path: 'user/register', component: RegisterComponent},
  { path: 'user/profile', component: ProfileComponent},
  { path: 'consultarInforme', component: ConsultarInformeComponent},
  { path: 'compararInforme/:id', component: CompararInformeComponent},
  { path: '**', component: Page404Component},

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
