import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CompararInformeComponent } from './comparar-informe.component';

describe('CompararInformeComponent', () => {
  let component: CompararInformeComponent;
  let fixture: ComponentFixture<CompararInformeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CompararInformeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CompararInformeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
