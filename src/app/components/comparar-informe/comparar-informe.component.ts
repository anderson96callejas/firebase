import { ActivatedRouteSnapshot } from '@angular/router';
import { InformeInterface } from './../../models/informe';
import { DataApiService } from './../../services/data-api.service';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Params } from '@angular/router';

@Component({
  selector: 'app-comparar-informe',
  templateUrl: './comparar-informe.component.html',
  styleUrls: ['./comparar-informe.component.css']
})
export class CompararInformeComponent implements OnInit {

  constructor(private dataApi: DataApiService, private route: ActivatedRoute ) { }

  public informe: InformeInterface = {};

  ngOnInit() {
    const idInforme = this.route.snapshot.params['id'];
    this.getDetails(idInforme);
  }

  getDetails(idInforme: string): void{
    this.dataApi.getOneInforme(idInforme).subscribe(informe => {
      this.informe = informe;
    });
  }

}
