import { DataApiService } from './../../services/data-api.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-consultar-informe',
  templateUrl: './consultar-informe.component.html',
  styleUrls: ['./consultar-informe.component.css']
})
export class ConsultarInformeComponent implements OnInit {

  constructor(private dataApi: DataApiService) { }

  public informes = [];
  public informe = '';

  ngOnInit() {
    this.dataApi.getAllInforme().subscribe(informes => {
      console.log('Informes', informes);
      this.informes = informes;
    })
  }

  onDeleteInforme(idInforme: string){
    console.log('Delete Informe', idInforme);
    this.dataApi.deleteInforme(idInforme);
  }

}