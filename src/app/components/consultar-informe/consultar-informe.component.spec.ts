import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ConsultarInformeComponent } from './consultar-informe.component';

describe('ConsultarInformeComponent', () => {
  let component: ConsultarInformeComponent;
  let fixture: ComponentFixture<ConsultarInformeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ConsultarInformeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ConsultarInformeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
