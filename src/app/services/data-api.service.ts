import { map } from 'rxjs/operators';
import { InformeInterface } from './../models/informe';
import { Injectable } from '@angular/core';
import { AngularFirestore, AngularFirestoreCollection, AngularFirestoreDocument } from '@angular/fire/firestore';
import { Observable } from 'rxjs/internal/Observable';

@Injectable({
  providedIn: 'root'
})
export class DataApiService {

  constructor(private afs: AngularFirestore) {
    this.informeCollection = afs.collection<InformeInterface>('informe');
    this.informes = this.informeCollection.valueChanges();
    
  }

  private informeCollection: AngularFirestoreCollection<InformeInterface>;
  private informes: Observable<InformeInterface[]>;
  private informeDoc: AngularFirestoreDocument<InformeInterface>;
  private informe: Observable<InformeInterface>;
 
  getAllInforme(){
    return this.informes = this.informeCollection.snapshotChanges()
    .pipe(map (changes => {
      return changes.map(action => {
        const data = action.payload.doc.data() as InformeInterface;
        data.id = action.payload.doc.id;
        return data;
      });
    }));
  }

  getOneInforme(idInforme: string){
    this.informeDoc = this.afs.doc<InformeInterface>(`informe/${idInforme}`);
    return this.informe = this.informeDoc.snapshotChanges().pipe(map(action => {
      if(action.payload.exists == false){
        return null; 
      } else {
        const data = action.payload.data() as InformeInterface;
        data.id = action.payload.id;
        return data;
      }
    }));
  }

  addInforme(informe: InformeInterface): void{
    this.informeCollection.add(informe);
  }

  updateInforme(informe: InformeInterface): void {
    let idInforme = informe.id;
    this.informeDoc = this.afs.doc<InformeInterface>(`informe/${idInforme}`);
    this.informeDoc.update(informe); 
  }

  deleteInforme(idInforme: string): void{
    this.informeDoc = this.afs.doc<InformeInterface>(`informe/${idInforme}`);
    this.informeDoc.delete();
  }
}
